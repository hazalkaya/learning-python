# https://github.com/Asabeneh/30-Days-Of-Python/blob/master/readme.md#exercise-level-3
# Finding Euclidian distance between (2, 3) and (10, 8) in a very simple way
# Formula √((a1-b1)^2 + (a2-b2)^2)
a=(2,3)
b=(10,8)
x_sq=((a[0] - b[0]) ** 2)
y_sq=((a[1] - b[1]) ** 2)
# We can use **0.5 for √
result=((x_sq + y_sq) ** 0.5)
print(x_sq)
print(y_sq)
print(result)

RESULT
------
64
25
9.433981132056603
