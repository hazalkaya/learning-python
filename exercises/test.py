#Given the variables x,y, and z, print the sum of the values that are not None.
#REFERENCE:https://learn-python.adamemery.dev/control_flow

# Try running multiple times to see different results
print(x, y, z)

sum=0

if(x==None):
    if(y==None):
        if(z==None):
            print(sum)
        else:
            print(sum + z)
    else:
        if(z==None):
            print(sum + y)
        else:
            print(sum + y + z) 
else:
    if(y==None):
        if(z==None):
            print(sum + x)
        else:
            print(sum + x + z)
    else:
        if(z==None):
            print(sum + y + x)
        else:
            print(sum + x + y + z)


## OUTPUT
#
#22 None 30
#52
#
#22 28 30
#80
#None None 29
#29
#19 25 19
#63
#12 None 20
#32
